var assert = require('assert');
describe('los estudiantes search', function() {
    it('Should search and enter teachers pages', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.waitForVisible('.buscador', 5000);

        var buscador = browser.element('.buscador');
        var profesroPreInput = buscador.element('.Select-placeholder');
        var profesorInput = buscador.element('input[role="combobox"]');

        profesroPreInput.click();
        profesorInput.keys('silvia taka');
        browser.waitForVisible('.Select-option', 10000);
        browser.keys("\uE007"); 
        browser.keys("\uE007"); 
        
        browser.waitForVisible('.nombreProfesor', 5000);
        var nombre = browser.element('.nombreProfesor').getText();
        
        expect(nombre).toBe('Silvia Takahashi');

        browser.back();
        browser.waitForVisible('.buscador', 5000);

        var profesroPreInput = buscador.element('.Select-placeholder');
        var profesorInput = buscador.element('input[role="combobox"]');

        profesroPreInput.click();
        profesorInput.keys('mario pinz');
        browser.waitForVisible('.Select-option', 10000);
        browser.keys("\uE007"); 
        browser.keys("\uE007"); 

        browser.waitForVisible('.nombreProfesor', 10000);
        var nombre = browser.element('.nombreProfesor').getText();

        expect(nombre).toBe('Mario Pinzon');
        
        browser.back();
        browser.waitForVisible('.buscador', 5000);

        var profesroPreInput = buscador.element('.Select-placeholder');
        var profesorInput = buscador.element('input[role="combobox"]');

        profesroPreInput.click();
        profesorInput.keys('agudelo mene');
        browser.waitForVisible('.Select-option', 10000);
        browser.keys("\uE007"); 
        browser.keys("\uE007"); 

        browser.waitForVisible('.nombreProfesor', 5000);
        var nombre = browser.element('.nombreProfesor').getText();

        expect(nombre).toBe('Laura Agudelo Meneses');
        
        browser.back();
        browser.waitForVisible('.buscador', 5000);

        var profesroPreInput = buscador.element('.Select-placeholder');
        var profesorInput = buscador.element('input[role="combobox"]');

        profesroPreInput.click();
        profesorInput.keys('qwiufbweqiufnqwepfnewiweqoife');
        browser.keys("\uE007"); 
        
        var noSearch = browser.isExisting('.nombreProfesor');
        expect(noSearch).toBe(false);
    });
});
