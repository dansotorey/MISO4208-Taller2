import { TourOfHeroesPage } from './app.po';

describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });

  it('should click on a hero', () => {
    page.navigateTo();
    expect(page.clickOnHero()).toBe(true);
  });

  it('should look for a heroe', () => {
    page.navigateTo();
    expect(page.lookForHeroWithName('Bombasto')).toEqual('Bombasto');
  });

  it('should edit one heroe', () => {
    page.editHero('Bombasto', 'Daniel Soto');
  });
});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new heroe', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

  it('should delete a heroe', () => {
    const currentHeroes = page.getAllHeroes().count();
    expect(page.deleteHero().count()).toBe(currentHeroes.then(n => n - 1));
  });

  it('should go to a heroes page', () => {
    expect(page.goToHeroePage()).toBe(true);
  });
});
