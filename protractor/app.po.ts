import {browser, by, element, ElementFinder} from 'protractor';

export class TourOfHeroesPage {
    navigateTo() {
        return browser.get('/');
    }

    getTop4Heroes() {
        return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
    }

    navigateToHeroes() {
        element(by.linkText('Heroes')).click();
    }

    getAllHeroes() {
        return element(by.tagName('my-heroes')).all(by.tagName('li'));
    }

    enterNewHeroInInput(newHero: string) {
        element(by.tagName('input')).sendKeys(newHero);
        element(by.buttonText('Add')).click();
    }

    lookForHeroWithName(name: string) {
        browser.wait(element(by.tagName('hero-search')).element(by.tagName('input')).sendKeys(name), 3000);
        return element(by.css('.search-result')).getText();
    }

    clickOnHero() {
      element(by.tagName('my-dashboard')).element(by.css('.module.hero')).click();
      return element(by.buttonText('Back')).isDisplayed();
    }

    deleteHero() {
        element(by.buttonText('x')).click();
        return this.getAllHeroes();
    }

    editHero(oldName: string, newName: string) {
        this.navigateTo();
        element(by.tagName('input')).sendKeys(oldName);
        element(by.css('.search-result')).click();
        element(by.tagName('hero-detail')).all(by.tagName('input')).sendKeys(newName);
        return element(by.buttonText('Save')).click();
    }

    goToHeroePage() {
      element(by.tagName('my-heroes')).element(by.tagName('li')).click();
      element(by.buttonText('View Details')).click();
      return element(by.buttonText('Back')).isDisplayed();
    }
}
